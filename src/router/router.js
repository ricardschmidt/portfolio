import { createWebHistory, createRouter } from "vue-router";

import MainNavbar from "../layout/MainNavbar.vue";
import MainFooter from "../layout/MainFooter.vue";

import Index from "../views/Index.vue";
import AboutMe from "../views/AboutMe.vue";
import Works from "../views/Works.vue";
import WorkPage from "../views/WorkPage.vue";

const routes = [
    {
		path: "/",
		name: "index",
		components: { default: Index, header: MainNavbar, footer: MainFooter },
		props: {
			header: { colorOnScroll: 200 },
		}
    },
    {
		path: "/about-me",
		name: "about me",
		components: { default: AboutMe, header: MainNavbar, footer: MainFooter },
		props: {
			header: { colorOnScroll: 200 },
		}
    },
    {
		path: "/works",
		name: "works",
		components: { default: Works, header: MainNavbar, footer: MainFooter },
		props: {
			header: { colorOnScroll: 200 },
		}
    },
    {
		path: "/works/:id",
		name: "work-page",
		components: { default: WorkPage, header: MainNavbar, footer: MainFooter },
		props: {
			header: { colorOnScroll: 100 },
		}
    },
];

const router = createRouter({
	history: createWebHistory(),
	routes,
	scrollBehavior() {
		return { top: 0 }
	},
});



  export default router;
