import { createApp } from 'vue'
import App from './App.vue'

import router from './router/router'

import "@fortawesome/fontawesome-free/css/all.min.css";
import './assets/styles/app.scss'

createApp(App)
.use(router)
.mount('#app')
