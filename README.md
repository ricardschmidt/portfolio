# Portfolio in Vue 3 + Tailwind

Portifólio com projetos contruidos e informaões a meu respeito.

## Quick start

Quick start options:

- Clone the repo: `git clone https://gitlab.com/ricardschmidt/portfolio.git`.
- [Download from Gitlab](https://gitlab.com/ricardschmidt/portfolio/-/archive/master/portfolio-master.zip).
- Run `npm install` or `yarn install`
- Run `npm run serve` or `yarn serve` to start a local development server


## Tecnologias usadas

[Vue.js (3)](https://vuejs.org/) as framework for development.
[Vue CLI 5.0.4](https://github.com/vuejs/vue-cli) for project scaffolding.
[Vue Router](https://router.vuejs.org/) for handling routes.
[Tailwind 3](https://tailwindcss.com/) as a general css foundation.
[Headless UI](https://headlessui.dev/) for some complex js components such as dialog, transitionChild.

## File Structure

Within the download you'll find the following directories and files:

```
Vue
|-- src
	|-- App.vue
	|-- main.js
	|-- index.css
	|-- assets
	|   |-- styles
	|   |	|-- scss
	|-- components
	|-- layout
	|-- router
	|-- views
```

